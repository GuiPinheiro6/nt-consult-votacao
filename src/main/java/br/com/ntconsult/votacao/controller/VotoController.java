package br.com.ntconsult.votacao.controller;

import br.com.ntconsult.votacao.dto.ResultadoVotacaoDto;
import br.com.ntconsult.votacao.model.Voto;
import br.com.ntconsult.votacao.shared.service.VotosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/voto")
public class VotoController {

    @Autowired
    private VotosService service;

    @GetMapping("/{id}")
    @ResponseBody
    public Voto getVoto(@PathVariable Long id) {
        return service.getVoto(id);
    }

    @PostMapping
    @ResponseBody
    public Voto insertVoto(@RequestBody Voto voto) {
        return service.insertVoto(voto);

    }

    @GetMapping("/resultado/{id}")
    @ResponseBody
    public ResultadoVotacaoDto getResultado(@PathVariable Long id) {
        return service.resultadoVotacao(id);
    }
}
