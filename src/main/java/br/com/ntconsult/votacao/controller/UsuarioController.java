package br.com.ntconsult.votacao.controller;

import br.com.ntconsult.votacao.model.Usuario;
import br.com.ntconsult.votacao.shared.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService service;

    @GetMapping("/{id}")
    @ResponseBody
    public Usuario getUser(@PathVariable Long id) {
        return service.getUser(id);
    }

    @PostMapping
    @ResponseBody
    public Usuario insertUser(@RequestBody Usuario user) {
        return service.insertUser(user);
    }
}

