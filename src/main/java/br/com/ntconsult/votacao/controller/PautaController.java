package br.com.ntconsult.votacao.controller;

import br.com.ntconsult.votacao.model.Pauta;
import br.com.ntconsult.votacao.shared.service.PautaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pauta")
public class PautaController {

    @Autowired
    private PautaService service;

    @GetMapping("/{id}")
    public Pauta getPauta(@PathVariable Long id) {
        return service.getPauta(id);
    }

    @PostMapping
    public Pauta insertPauta(@RequestBody Pauta pauta) {
        return service.insertPauta(pauta);
    }

    @PostMapping("/sessao")
    public Pauta insertSessao(@RequestBody Pauta pauta) {
        return service.insertSessao(pauta);
    }
}


