package br.com.ntconsult.votacao.repository;

import br.com.ntconsult.votacao.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
