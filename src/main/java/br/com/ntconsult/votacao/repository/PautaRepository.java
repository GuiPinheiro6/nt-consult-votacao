package br.com.ntconsult.votacao.repository;

import br.com.ntconsult.votacao.model.Pauta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PautaRepository extends JpaRepository<Pauta, Long> {

}

